# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.delete(str.downcase)
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  x = str.length

  if x.even?
    str[x/2-1..x/2]
  elsif x.odd?
    str[x/2]
  end

end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.scan(/[aeiou]/).uniq.count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).to_a.reduce { |acc,num| acc * num }
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")

  result = String.new
  arr.each do |ch|
    result += ch
    result += separator unless ch == arr.last
  end
  result
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  results = ""

  str.downcase.chars.each_with_index do |ch,idx|
    if idx.odd?
      results += ch.upcase
    else
      results += ch
    end
  end
  results

end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  results = []

  str.split.each do |word|
    if word.length >= 5
      results << word.reverse
    else
      results << word
    end
  end
  results.join(" ")

end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  results = []

  (1..n).to_a.each do |num|
    if num % 15 == 0
      results << "fizzbuzz"
    elsif num % 3 == 0
      results << "fizz"
    elsif num % 5 == 0
      results << "buzz"
    else
      results << num
    end
  end
  results


end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if factors(num).length == 2
    return true
  else
    return false
  end
end


# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []

  one_to_num = (1..num).to_a
  one_to_num.map do |int|
    if num % int == 0
      factors << int
    end
  end
  factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  results = []
  factors(num).each do |num|
    results << num if prime?(num)
  end
  results
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = arr.select { |int| int.even?}
  odds = arr.select { |int| int.odd? }

  if evens.length > odds.length
    return  odds[0]
  else
    return evens[0]
  end
end
